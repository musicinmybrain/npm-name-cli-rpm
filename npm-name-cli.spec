%global npm_name npm-name-cli

Name:           %{npm_name}
Version:        3.0.0
Release:        1%{?dist}
Summary:        Check whether a package or organization name is available on npm

# License of npm-name-cli is MIT; others come from bundled dependencies. See
# the license file %%{npm_name}-%%{version}-bundled-licenses.txt for a list of
# licenses in NPM format. Each bundled dependency has the license specified in
# the "license" key of its package.json file.
License:        MIT and ASL 2.0 and BSD and (BSD or MIT or ASL 2.0) and CC0 and CC-BY and ISC and (MIT or CC0) and MPLv2.0
URL:            https://github.com/sindresorhus/%{npm_name}
# The tests are not included in the NPM tarball. However, they all require
# Internet access, so we omit them entirely. This also means we require only
# the prod dependency bundle, not the dev one.
Source0:        https://registry.npmjs.org/%{npm_name}/-/%{npm_name}-%{version}.tgz
# Created with (from nodejs-packaging RPM):
# nodejs-packaging-bundler %%{npm_name} %%{version}
Source1:        %{npm_name}-%{version}-nm-prod.tgz
Source2:        %{npm_name}-%{version}-bundled-licenses.txt
# Hand-written man page
Source3:        npm-name.1

ExclusiveArch:  %{nodejs_arches} noarch
BuildArch:      noarch

BuildRequires:  nodejs-devel
BuildRequires:  symlinks

Requires:       nodejs

%description
The npm-name command-line tool checks whether a package or organization name is
available on npm.

Why would I use npm-name rather than npm’s built-in search?

1. Nicer & simpler output
2. Squatter detection (https://github.com/sholladay/squatter)
3. Supports checking the availability of organization names
4. Performance


%prep
%setup -q -n package

cp %{SOURCE2} .
# Set up bundled runtime (prod) node modules.
tar -xzf %{SOURCE1}
mkdir -p node_modules
pushd node_modules
ln -s ../node_modules_prod/* .
ln -s ../node_modules_prod/.bin .
popd

# Fix shebang lines in executables. For some reason, brp-mangle-shebangs does
# not seem to do this under %%nodejs_sitelib.
find . -type f -perm /0111 |
  while read -r fn
  do
    if head -n 1 "${fn}" | grep -E '^#!%{_bindir}/env[[:blank:]]+' >/dev/null
    then
      sed -r -i '1s/env +//' "${fn}"
    fi
  done


# Nothing to build


%install
install -d %{buildroot}%{nodejs_sitelib}/%{npm_name}
cp -rp \
    package.json \
    cli.js \
    node_modules node_modules_prod \
    %{buildroot}%{nodejs_sitelib}/%{npm_name}

install -d %{buildroot}%{_bindir}
# Create an absolute symlink in the buildroot, then convert it to a relative
# one that will still resolve after installation. Otherwise, to create a
# relative symlink, we would have to know how deeply nested %%nodejs_sitelib
# is, which breaks the abstraction of using a macro.
ln -sf %{buildroot}%{nodejs_sitelib}/%{npm_name}/cli.js \
    %{buildroot}%{_bindir}/npm-name
symlinks -c -o %{buildroot}%{_bindir}/npm-name

install -d %{buildroot}%{_mandir}/man1
install -t %{buildroot}%{_mandir}/man1 -p -m 0644 %{SOURCE3}


# No %%check section, as all tests require Internet access


%files
%doc readme.md
%license license %{npm_name}-%{version}-bundled-licenses.txt
%{nodejs_sitelib}/%{npm_name}
%{_bindir}/npm-name
%{_mandir}/man1/npm-name.1*


%changelog
* Fri Jan 22 2021 Benjamin A. Beasley <code@musicinmybrain.net> - 3.0.0-1
- Initial package
